﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C16_Ex01_2
{
    public class Program
    {
        static void Main()
        {
            Sand_Machine.PrintSandMachine(5);
        }
    }


    public static class Sand_Machine
    {
        public static void PrintSandMachine(int Height)
        {
            StringBuilder Machine = new StringBuilder();
            for (int i = Height; i > 0; i -= 2)
            {
                Machine.Append(' ', Height - i / 2);
                Machine.Append('*', i);
                Machine.Append(Environment.NewLine);
            }
            for (int i = 3; i <= Height; i += 2)
            {
                Machine.Append(' ', Height - i / 2);
                Machine.Append('*', i);
                Machine.Append(Environment.NewLine);
            }
            Console.WriteLine(Machine);
        }
    }
}
