﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C16_Ex01_1
{
    public class Program
    {
        static void Main()
        {
            BinaryConverter.GetInputFromUser();
        }
    }



    static class BinaryConverter
    {
        public static void GetInputFromUser()
        {
            int CurrentNumber = 0;
            string CurrentInput = string.Empty;
            Console.WriteLine("Please enter 4 numbers with length of 3");
            List<int> Numbers = new List<int>();
            List<string> BinaryNumbers = new List<string>();
            for (int i = 0; i < 4; i++)
            {
                CurrentInput = Console.ReadLine();
                if (int.TryParse(CurrentInput, out CurrentNumber))
                {
                    if (CurrentInput.Length == 3)
                    {
                        Numbers.Add(CurrentNumber);
                        BinaryNumbers.Add(ToBinary(CurrentNumber));
                    }
                    else
                    {
                        Console.WriteLine("Sorry, You must enter a no more then 3 digits length number. ");
                        i--;
                    }
                }
                else
                {
                    Console.WriteLine("Sorry, You must enter a 3 digit number.");
                    i--;
                }
            }
            Console.WriteLine();
            Console.WriteLine("\t Converted Numbers:");
            foreach (string Number in BinaryNumbers)
            {
                Console.WriteLine("\t " + Number);
            }
            Console.WriteLine();
            Statistics(Numbers, BinaryNumbers);
        }

        static void Statistics(List<int> Nums, List<string> BinaryNums)
        {
            Console.WriteLine("\t Statistics : ");
            Console.WriteLine("\t Binary Average is : " + BinaryAvg(BinaryNums));
            Console.WriteLine("\t Numbers Average is : " + NumAvg(Nums));
            Console.WriteLine("\t Number of Ascending numbers : " + IsAscending(Nums));
            Console.WriteLine("\t Number of Ascending numbers : " + IsDescending(Nums));
            Console.WriteLine();
        }

        static double BinaryAvg(List<string> BinaryNums)
        {
            double BinaryDigitAvg = 0;
            foreach (string Number in BinaryNums)
            {
                BinaryDigitAvg += Number.Length;
            }
            BinaryDigitAvg /= 4;
            return BinaryDigitAvg;
        }
        static double NumAvg(List<int> Nums)
        {
            double DigitAvg = 0;
            foreach (int Number in Nums)
            {
                DigitAvg += Number;
            }
            DigitAvg /= 4;
            return DigitAvg;
        }
        static string ToBinary(int Decimal)
        {
            int Remainder;
            string Result = string.Empty;
            while (Decimal > 0)
            {
                Remainder = Decimal % 2;
                Decimal /= 2;
                Result = Remainder.ToString() + Result;
            }
            return Result;
        }

        static int IsAscending(List<int> Nums)
        {
            int AscendCount = 0;
            foreach (int Number in Nums)
            {
                if (Number / 100 < Number / 10 % 10 && Number / 10 % 10 < Number % 10)
                    AscendCount++;
            }
            return AscendCount;
        }

        static int IsDescending(List<int> Nums)
        {
            int DesCount = 0;
            foreach (int Number in Nums)
            {
                if (Number / 100 > Number / 10 % 10 && Number / 10 % 10 > Number % 10)
                    DesCount++;
            }
            return DesCount;
        }

    }
}
