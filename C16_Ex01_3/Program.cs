﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C16_Ex01_3
{
    public class Program
    {
        static void Main()
        {
            Advanced_Sand_Machine.GetNumOfRows();
        }
    }


    static class Advanced_Sand_Machine
    {
        public static void GetNumOfRows()
        {
            Console.WriteLine("Please enter the height of your sand machine.");
            int NumOfRows = 0;
            bool flag = false;
            while (!flag)
            {
                if (int.TryParse(Console.ReadLine(), out NumOfRows))
                {
                    if (NumOfRows <= 0)
                    {
                        Console.WriteLine("Sorry, You must enter a Positive Number");
                    }
                    else
                    {
                        if (NumOfRows % 2 == 0)
                            NumOfRows++;
                        flag = true;
                    }
                }
                else
                {
                    Console.WriteLine("Sorry, We accept only Positive Numbers.");
                }
            }
            C16_Ex01_2.Sand_Machine.PrintSandMachine(NumOfRows);
            flag = false;
        }
    }
}
