﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C16_Ex01_4
{
    public class Program
    {
        static void Main()
        {
            StringHandler.GetInput();
        }
    }

    static class StringHandler
    {
        static bool IsPali(string input)
        {
            int length = input.Length;
            for (int i = 0; i < length / 2; i++)
            {
                if (input[i] != input[length - i - 1])
                    return false;
            }
            return true;
        }
        public static void GetInput()
        {
            bool flag = false;
            int IntInput = 0;
            Console.WriteLine("Please enter a 10 letter string");
            while (!flag)
            {
                string input = Console.ReadLine();
                if(input.Length == 10)
                {
                    if(int.TryParse(input,out IntInput))
                    {
                        Console.WriteLine("Is Palindrome : " + IsPali(input));
                        double Avg = 0;
                        for (int i = 0; i < 10; i++)
                        {
                            Avg += IntInput % 10;
                            IntInput /= 10;
                        }
                        Avg /= 10;
                        Console.WriteLine("The Average is " + Avg);
                        flag = true;
                    }
                    else
                    {
                        if(IsValidString(input))
                        {
                            Console.WriteLine("Is Palindrome : " + IsPali(input));
                            int NumOfCapital = 0;
                            foreach (char Letter in input)
                            {
                                if (Letter.ToString() == Letter.ToString().ToUpper())
                                    NumOfCapital++;
                            }
                            Console.WriteLine("Number of Capital Letters : " + NumOfCapital);
                            flag = true;
                        }
                        else
                            Console.WriteLine("Sorry, can't mix letters with numbers.");
                    }
                }
                else
                    Console.WriteLine("Number length must be exactly 10 chars");
            }
        }

        static bool IsValidString(string input)
        {
            int OutputNum = 0;
            foreach (char Letter in input)
            {
                if (int.TryParse(Letter.ToString(), out OutputNum))
                    return false;
            }
            return true;
        }
    }
}
